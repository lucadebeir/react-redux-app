import firebase from 'firebase/app'
import 'firebase/firestore'
import 'firebase/auth'

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
var firebaseConfig = {
    apiKey: "AIzaSyCFOSghFdpF1NmOJ1BOLVN7CokGesQ4y4U",
    authDomain: "tuto-react-redux-92f31.firebaseapp.com",
    databaseURL: "https://tuto-react-redux-92f31.firebaseio.com",
    projectId: "tuto-react-redux-92f31",
    storageBucket: "tuto-react-redux-92f31.appspot.com",
    messagingSenderId: "564991512953",
    appId: "1:564991512953:web:e589ec127d4c3a5a77dfa8",
    measurementId: "G-0MNLRTSD8E"
};
// Initialize Firebase
firebase.initializeApp(firebaseConfig);
firebase.firestore().settings({ timestampsInSnapshots: true });

export default firebase;